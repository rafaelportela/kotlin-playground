package flow.sequence

fun foo(): Sequence<Int> = sequence {
    for (i in 1..3) {
        // entire thread will wait
        Thread.sleep(100)
        yield(i)
    }
}

fun main() {
    foo().forEach { value -> println(value) }
}