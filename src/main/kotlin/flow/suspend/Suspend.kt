package flow.suspend

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

suspend fun foo(): List<Int> {

    // Thread.sleep would stop the entire thread.
    // Instead, a suspended function can be suspended (delayed) even when running in the same thread with other
    // suspended functions or coroutines.
    delay(1000)
    return listOf(1, 2, 3)
}

// `runBlocking` wraps coroutines or suspended functions and will block the main function waiting for results.
fun main() = runBlocking<Unit> {
    foo().forEach { value -> println(value) }
}
