package flow.async

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.random.Random
import kotlin.system.measureTimeMillis

suspend fun expensiveRandomNumber(): Int {

    // delay can only be used in suspend functions or coroutines
    // therefore, the fun needs the suspend modifier
    delay(200)

    return Random.nextInt(0, 10)
}

// async coroutine build
fun somethingUsefulOneAsync() = GlobalScope.async {
    expensiveRandomNumber()
}

fun somethingUsefulTwoAsync() = GlobalScope.async {
    expensiveRandomNumber()
}

fun main() {
    val time = measureTimeMillis {

        // not a suspend function, can be called from anywhere
        val one = somethingUsefulOneAsync()
        val two = somethingUsefulTwoAsync()

        runBlocking {
            // suspend function `await` should be called from a coroutine or from another suspend function
            println("The answer is ${one.await()}")
        }
    }
    println("Completed in $time ms")
}