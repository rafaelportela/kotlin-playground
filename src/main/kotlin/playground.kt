import java.lang.NumberFormatException

fun yetAnotherParseInt(s: String): Int? {

    return try {
        Integer.parseInt(s)
    } catch (e: NumberFormatException) {
        null
    }
}

fun main() {
    println(yetAnotherParseInt("10"))
    println(yetAnotherParseInt("a10"))
}