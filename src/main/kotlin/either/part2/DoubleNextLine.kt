package either.part2

import either.*
import java.io.BufferedReader
import java.io.IOException

// parse string returning Either the value or error message
fun parseInt(s: String): Either<String, Int> = try {
    Right(Integer.parseInt(s))
} catch (ex: Exception) {
    Left(ex.message ?: "No message")
}

fun oldStyleDoubleString(s: String): Either<String, Int> {
    val result: Either<String, Int> = parseInt(s)
    return when (result) {
        is Right -> Right(2 * result.r)
        is Left -> result
    }
}

// Returns either the double the value of parsed int, or an error message
fun doubleString(s: String): Either<String, Int> = parseInt(s).map { 2 * it }

fun BufferedReader.eitherReadLine(): Either<String, String> =
    try {
        val line = this.readLine()
        if (line == null)
            Left("No more lines")
        else
            Right(line)
    } catch (x: IOException) {
        Left(x.message ?: "No message")
    }

// Returns either a Right with the doubleString of a result,
// or a Left if eitherReadLine fails.
fun doubleNextLine(reader: BufferedReader): Either<String, Int> =
    reader.eitherReadLine().flatMap { doubleString(it) }
