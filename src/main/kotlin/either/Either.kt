package either

//
// part 2
//

sealed class Either<out L, out r>

data class Left<out L>(val l: L) : Either<L, Nothing>()
data class Right<out R>(val r: R) : Either<Nothing, R>()


// Maps an Either object to another Either object,
// the new Right value is defined by the f fun param.
inline fun <L, R1, R2> Either<L, R1>.map(f: (R1) -> R2): Either<L, R2> =
    when (this) {
        is Right -> Right(f(this.r))
        is Left -> this
    }

// Unpacks our value and uses it to invoke f,
// which returns Either showing that it may also fail
inline fun <L, R1, R2> Either<L, R1>.flatMap(f: (R1) -> Either<L, R2>): Either<L, R2> =
    when(this) {
        is Right -> f(this.r)
        is Left -> this
    }

//
// part 3 - Result and Fold
//

// Fold: unwraps the Either value based on one the f params
// It takes 2 functions and returns the result of
// calling the first if the Either is Left,
// or the second if it's Right
inline fun <L, R, T> Either<L, R>.fold(fl: (L) -> T, fr: (R) -> T): T =
    when (this) {
        is either.Right -> fr(this.r)
        is either.Left -> fl(this.l)
    }

// helper to keep exceptions on errors
inline fun <R> resultOf(f: () -> R): Either<Exception, R> = try {
    Right(f())
} catch (e: Exception) {
    Left(e)
}
