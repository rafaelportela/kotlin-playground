package either.part3

import either.*

fun parseInt(s: String): Either<Exception, Int> = resultOf { Integer.parseInt(s) }

fun usingFold() {

    // fold expects 2 func params: one to process Right, other to process Left
    parseInt(readLine() ?: "").fold(
        fr = { int -> println("Your number $int") },
        fl = { exception -> println("I couldn't read your number because $exception") }
    )
}

// extension on Either with a method I supposedly use often
inline fun <L, R, T> Either<L, R>.assertSuccess(f: (R) -> T) =
    when (this) {
        is Right -> f(this.r)
        is Left -> error("Value was not a Right, but a Left of ${this.l}")
    }

fun someOperation() = Right("banana")

fun usingLocalExtensionOfEither() {

    val result: Either<Exception, String> = someOperation()

    result.assertSuccess {
        // requires 'import kotlin.test.assertEquals'
        // assertEquals("banana", it)
    }

    // identify fun as param: equivalent to { it -> it }
    val rightValue: String = result.assertSuccess { it }
}
